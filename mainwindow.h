#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class QDir;
class QStringListModel;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void doReplace();
    void addDir();

private:
    void processDir(const QDir &srcDir, const QStringList &listDstPaths, const QString &subdir = QString());

private:
    Ui::MainWindow *ui;
    QStringListModel *m_model;
};

#endif // MAINWINDOW_H
