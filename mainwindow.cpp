#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDir>
#include <QFileDialog>
#include <QtDebug>
#include <QStringListModel>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_model(new QStringListModel(this))
{
    ui->setupUi(this);

    connect(ui->actionAdd_source_directory, SIGNAL(triggered(bool)), this, SLOT(addDir()));
    connect(ui->actionProcess_files, SIGNAL(triggered(bool)), this, SLOT(doReplace()));

    ui->listView->setModel(m_model);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::doReplace()
{
    const QString &sDstDir = QFileDialog::getExistingDirectory(this, tr("Select destination directory"), "C:/Projects/FormOCR_CS");
    if (sDstDir.isEmpty())
        return;

	QApplication::setOverrideCursor(Qt::WaitCursor);

    // List destination files
    QDir dirDst(sDstDir);
    const QStringList &listDstFiles = dirDst.entryList(QStringList()<<"*.cpp"<<"*.h", QDir::Files);
    QStringList listDstPaths;
    const int nDstFiles = listDstFiles.size();
    for (int i = 0; i < nDstFiles; ++i)
        listDstPaths.push_back(dirDst.absoluteFilePath(listDstFiles[i]));

    // process all dirs
    const int count = m_model->rowCount();
    for (int i = 0; i < count; ++i)
    {
        processDir(QDir(m_model->index(i, 0).data().toString()), listDstPaths);
    }

	QApplication::restoreOverrideCursor();
}

void MainWindow::addDir()
{
    const QString &sDir = QFileDialog::getExistingDirectory(this, tr("Select directory to add"), "C:/Projects/FormOCR_CS");
    if (!sDir.isEmpty())
    {
        m_model->insertRow(m_model->rowCount());
        m_model->setData(m_model->index(m_model->rowCount() - 1), sDir);
    }
}

void MainWindow::processDir(const QDir &srcDir, const QStringList &listDstPaths, const QString &subdir)
{
    const QStringList &listFiles = srcDir.entryList(QStringList()<<"*.cpp"<<"*.h", QDir::Files);
    const QStringList &listDirs = srcDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);

    const int dirCount = listDirs.size();
    for (int i = 0; i < dirCount; ++i)
    {
        if (listDirs[i].startsWith("."))
            continue;

        QDir subDir(srcDir);
        subDir.cd(listDirs[i]);
        processDir(subDir, listDstPaths, subdir + listDirs[i] + "/");
    }

    // Cycle destination files
    const int nDstFiles = listDstPaths.size();
    for (int d = 0; d < nDstFiles; ++d)
    {
        // Read the file
        QFile file(listDstPaths[d]);
        if (!file.open(QIODevice::ReadOnly))
        {
            qWarning()<<"Failed to open"<<listDstPaths[d]<<"for reading";
            continue;
        }

        QString sContent = file.readAll();
        file.close();

        // Replace includes
        const int fileCount = listFiles.size();
        for (int i = 0; i < fileCount; ++i)
        {
            const QString &sIncludeName = listFiles[i];
            if (sIncludeName.startsWith("moc_")) // skip moc files
                continue;

            sContent.replace(QString("#include \"%1\"").arg(sIncludeName),                             
				QString("#include <%1%2>").arg(subdir).arg(sIncludeName), Qt::CaseInsensitive);
        }

        // Save the file
        if (!file.open(QIODevice::WriteOnly))
        {
            qWarning()<<"Failed to open"<<listDstPaths[d];
            continue;
        }

        file.write(sContent.toUtf8());
    }
}
