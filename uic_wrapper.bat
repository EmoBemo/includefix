@echo off
SetLocal EnableDelayedExpansion
(set PATH=C:\QtBuild\qt5.6.1_vs2008\qtbase\lib;!PATH!)
if defined QT_PLUGIN_PATH (
    set QT_PLUGIN_PATH=C:\qtbuild\qt5.6.1_vs2008\qtbase\plugins;!QT_PLUGIN_PATH!
) else (
    set QT_PLUGIN_PATH=C:\qtbuild\qt5.6.1_vs2008\qtbase\plugins
)
C:\QtBuild\qt5.6.1_vs2008\qtbase\bin\uic.exe %*
EndLocal
